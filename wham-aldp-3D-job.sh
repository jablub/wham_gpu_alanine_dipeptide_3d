#PBS -N WHAM_GPU_ALANINE_DIPEPTIDE_3D
#PBS -q GPUQ
#PBS -l nodes=1:ppn=1:seriesGPU


# Please leave the hostname command here for troubleshooting purposes.
hostname


# Your science stuff goes here:
/home/apotgieter/WHAM_GPU_ALANINE_DIPEPTIDE_3D/B40x40x40/wham-gpu-3D.sh
/home/apotgieter/WHAM_GPU_ALANINE_DIPEPTIDE_3D/B40x80x160/wham-gpu-3D.sh
#/home/apotgieter/WHAM_GPU_ALANINE_DIPEPTIDE_3D/B160x160x160/wham-gpu-3D.sh